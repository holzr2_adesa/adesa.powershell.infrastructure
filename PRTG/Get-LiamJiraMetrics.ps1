$secpasswd = ConvertTo-SecureString "abc1abc" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("helpdesk", $secpasswd)

$date = get-date -Format "yyyy-MM-dd"

$query_resolved_LS = 'project = HD AND status = "Resolved" AND assignee in (saltl) AND ' + "created >= $date"
$query_closed_LS = 'project = HD AND status = "Closed" AND assignee in (saltl) AND ' + "created >= $date"
$query_resolved_CD = 'project = HD AND status = "Resolved" AND assignee in (daviesc) AND ' + "created >= $date"
$query_closed_CD = 'project = HD AND status = "Closed" AND assignee in (daviesc) AND ' + "created >= $date"

$unresolved_SD = [string](Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Triage", New, "In Progress") AND assignee in ("saltl", "daviesc")' | Measure-Object).Count

$unresolved = [string](Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Triage", New, "In Progress")' | Measure-Object).Count

$unresolved_LS = [string](Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Triage", New, "In Progress") AND assignee in (saltl)' | Measure-Object).Count
$resolved_today_LS = [string](Get-JiraIssue -Credential $cred -Query $query_resolved_LS | Measure-Object).Count
$closed_today_LS = [string](Get-JiraIssue -Credential $cred -Query $query_closed_LS | Measure-Object).Count

$unresolved_CD = [string](Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Triage", New, "In Progress") AND assignee in (daviesc)' | Measure-Object).Count
$resolved_today_CD = [string](Get-JiraIssue -Credential $cred -Query $query_resolved_CD | Measure-Object).Count
$closed_today_CD = [string](Get-JiraIssue -Credential $cred -Query $query_closed_CD | Measure-Object).Count



$new_count = [string](Get-JiraIssue -Credential $cred -Query '(project = HD AND status = New AND resolution = Unresolved AND assignee in (EMPTY)) AND NOT reporter = "Smart Alert"' | Measure-Object).Count

Write-Host "<prtg>"  
Write-Host "<result>"
Write-Host "<channel>Liam Salt Unresolved</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$unresolved_LS</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Liam Salt Resolved Today</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$resolved_today_LS</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Liam Salt Closed Today</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$closed_today_LS</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Craig Davies Unresolved</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$unresolved_CD</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Craig Davies Resolved Today</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$resolved_today_CD</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Craig Davies Closed Today</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$closed_today_CD</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Unresolved (Assigned to Service Desk)</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$unresolved_SD</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Unresolved (All)</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$unresolved</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Unassigned tickets</channel>"
Write-Host "<float>1</float>"
Write-Host "<value>$new_count</value>"
Write-Host "</result>"
Write-Host "</prtg>"

$out