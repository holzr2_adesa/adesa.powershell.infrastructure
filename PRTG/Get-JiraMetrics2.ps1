$secpasswd = ConvertTo-SecureString "abc1abc" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("helpdesk", $secpasswd)

$new_span = @()
Get-JiraIssue -Credential $cred -Query '(project = HD AND status = New AND resolution = Unresolved AND assignee in (EMPTY)) AND NOT reporter = "Smart Alert"' | ForEach-Object {
    $new_span += (New-TimeSpan -Start $_.Created -End (Get-Date)).TotalSeconds
}
$new_avg = ($new_span | Measure-Object -Average).Average

$triage_span = @()
Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Triage")' | ForEach-Object {
    $triage_span += (New-TimeSpan -Start $_.Updated -End (Get-Date)).TotalSeconds
}
$triage_avg = ($triage_span | Measure-Object -Average).Average

$inprogress_span = @()
Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("In Progress")' | ForEach-Object {
    $inprogress_span += (New-TimeSpan -Start $_.Updated -End (Get-Date)).TotalSeconds
}
$inprogress_avg = ($inprogress_span | Measure-Object -Average).Average

$resolved_span = @()
Get-JiraIssue -Credential $cred -Query 'project = HD AND status in ("Resolved")' | ForEach-Object {
    $resolved_span += (New-TimeSpan -Start $_.Updated -End (Get-Date)).TotalSeconds
}
$resolved_avg = ($resolved_span | Measure-Object -Average).Average

Write-Host "<prtg>"  
Write-Host "<result>"
Write-Host "<channel>Average time in New</channel>"
Write-Host "<float>1</float>"
Write-Host "<unit>TimeSeconds</unit>"
Write-Host "<value>$new_avg</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Average time in Triage</channel>"
Write-Host "<float>1</float>"
Write-Host "<unit>TimeSeconds</unit>"
Write-Host "<value>$triage_avg</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Average time in In Progress</channel>"
Write-Host "<float>1</float>"
Write-Host "<unit>TimeSeconds</unit>"
Write-Host "<value>$inprogress_avg</value>"
Write-Host "</result>"
Write-Host "<result>"
Write-Host "<channel>Average time in Resolved</channel>"
Write-Host "<float>1</float>"
Write-Host "<unit>TimeSeconds</unit>"
Write-Host "<value>$resolved_avg</value>"
Write-Host "</result>"
Write-Host "</prtg>"