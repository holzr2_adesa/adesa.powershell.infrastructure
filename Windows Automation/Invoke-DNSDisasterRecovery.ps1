<#
.SYNOPSIS
  This script performs a Disaster Recovery/Disaster Recovery Rollback by changing internal DNS records
.DESCRIPTION
  <Brief description of script>
.PARAMETER ZoneName
  String input, name of the DNS zone you are changing
.PARAMETER Path
  String input, path to the CSV of your DNS entries
.PARAMETER Rollback
  Switch, reverts changes made by a DNS disaster recovery
.INPUTS
  CSV file of DNS entries, with entry name, old IP and new IP
.NOTES
  Version:        1.0
  Author:         Robert Holz
  Creation Date:  12/04/2017
  Purpose/Change: Initial script development
.EXAMPLE
  Changes the DNS entries to the new IP addresses listed
  
  .\Invoke-DNSDisasterRecovery.ps1 -ZoneName "sfs.local" -Path "C:\scripts\DNS_entries.csv"
.EXAMPLE
  Performs a disaster recovery DNS rollback, reverting teh changes made my disaster recovery
  
  .\Invoke-DNSDisasterRecovery.ps1 -ZoneName "sfs.local" -Path "C:\scripts\DNS_entries.csv" -Rollback
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
    [switch]$Rollback,
    [Parameter(mandatory = $true)][string]$ZoneName,
    [Parameter(mandatory = $true)][string]$Path
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

Import-Module ActiveDirectory

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$output = @()

$head = "<style>"
$head = $head + "BODY{background-color:peachpuff;}"
$head = $head + "TABLE{border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}"
$head = $head + "TH{border-width: 1px;padding: 0px;border-style: solid;border-color: black;background-color:thistle}"
$head = $head + "TD{border-width: 1px;padding: 0px;border-style: solid;border-color: black;background-color:PaleGoldenrod}"
$head = $head + "</style>"

$recipiant = "adesait@adesa.co.uk"
$sender = "noreply@adesa.co.uk"
$emailrelay = "adesa-emailservices.sfs.local"

#-----------------------------------------------------------[Functions]------------------------------------------------------------

function Set-Record {
    Param (
        [string]$DC,
        [string]$oldip,
        [string]$newip,
        [string]$ZN,
        [string]$hostname
    )
    $NewObj = $OldObj = Get-DnsServerResourceRecord -Name $hostname -ZoneName $ZN -RRType "A" -ComputerName $DC
    if ($Rollback) {
        $NewObj.RecordData = $oldip
    }
    else {
        $NewObj.RecordData = $newip
    }
    Set-DnsServerResourceRecord -NewInputObject $NewObj -OldInputObject $OldObj -ZoneName $ZN -ComputerName $DC
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------

Get-ADDomainController -Filter * | ForEach-Object {
    if ((Test-NetConnection $_.HostName -InformationLevel Quiet) -eq $true) {
        $domaincontroller = $_.HostName
        $out = New-Object psobject
        Import-Csv $Path | ForEach-Object {
            Set-Record -DC $domaincontroller -oldip $_.old_ip -newip $_.new_ip -ZN $ZoneName -hostname $_.HostName
        }
        $out | Add-Member -MemberType NoteProperty -Name "RecordName" -Value $_.hostname
        if ($Rollback) {
            $out | Add-Member -MemberType NoteProperty -Name "OldIP" -Value $_.new_ip
            $out | Add-Member -MemberType NoteProperty -Name "NewIP" -Value $_.old_ip
        }
        else {
            $out | Add-Member -MemberType NoteProperty -Name "OldIP" -Value $_.old_ip
            $out | Add-Member -MemberType NoteProperty -Name "NewIP" -Value $_.new_ip
        }
        $out | Add-Member -MemberType NoteProperty -Name "DNSServer" -Value $domaincontroller
        $output += $out
    }
}

$output_html = $output | ConvertTo-Html -Head $head
$body = "<h1>Hello ADESA IT Team,&nbsp;</h1>"
if ($Rollback) {
    $body += "<p>We have just performed a Disaster Recovery Failover Rollback, please see the below list of DNS records that have been changed</p>"
}
else {
    $body += "<p>We have just performed a Disaster Recovery Failover, please see the below list of DNS records that have been changed</p>"
}
$body += "<p>&nbsp;</p>"
$body += $output_html
$body += "<p>&nbsp;</p>"

Send-MailMessage -Body $body -BodyAsHtml -SmtpServer $emailrelay -From $sender -To $recipiant

$output | Out-GridView